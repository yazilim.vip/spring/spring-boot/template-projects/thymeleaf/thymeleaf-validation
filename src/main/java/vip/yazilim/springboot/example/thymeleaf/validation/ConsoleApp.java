package vip.yazilim.springboot.example.thymeleaf.validation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsoleApp {

    public static void main(String[] args) {
        SpringApplication.run(ConsoleApp.class, args);
        System.out.println("\n This is the simplest Spring Boot Application \n");
    }

}
