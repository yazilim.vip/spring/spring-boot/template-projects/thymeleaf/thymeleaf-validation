package vip.yazilim.springboot.example.thymeleaf.validation;


import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class PageController {

    @GetMapping("/login")
    public String login(PersonForm personForm) {
        return "login";
    }


    @PostMapping("/login")
    public String checkPersonInfo(@Valid PersonForm personForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult);
            return "login";
        }
        return "true";
    }
}
